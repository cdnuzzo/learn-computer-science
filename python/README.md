There are some more theory related videos on Real Python's YouTube channel that are taken from paid courses. Here is a list of the free ones I found useful (unless otherwise denoted by the `$` for paid):

- [What is a Thread?](https://youtu.be/kM4oZTJaO8k)
- [Threading](https://realpython.com/lessons/daemon-threads/) `$`
- [Python With Concurrency](https://realpython.com/lessons/python-concurrency-overview/) `$`
- [Concurrency](https://youtu.be/OtdL6jeyEE4)
- [Memory Management](https://youtu.be/Vs8aN5wSyxA)
- [Memory Management in Python](https://realpython.com/courses/how-python-manages-memory/) `$`


- [Understanding Search Algorithms in Python](https://youtu.be/XgteJLurvB4)

- [Web Scraping](https://realpython.com/lessons/web-scraping-bs-overview/) `$`
- [Python and Requests to Scrape Sites](https://youtu.be/u8f0OAh-tck)

- [Pointers and Objects](https://realpython.com/courses/pointers-python/) `$`


- [DevOps With Python](https://realpython.com/courses/pointers-python/) `$`

- [Flask by Example](https://realpython.com/learning-paths/flask-by-example/)

- [Choosing a Project Platform](https://youtu.be/FD4xR3Rmg2Q)
- [Grow Project Portfolio](https://realpython.com/courses/intermediate-project-ideas/) `$`