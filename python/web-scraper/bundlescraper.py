#!/usr/bin/env python3

import requests
from bs4 import BeautifulSoup

url = "https://www.humblebundle.com/books/hacking-101-no-starch-press-books"
resp = requests.get(url)

soup = BeautifulSoup(resp.text, 'html.parser')

# bundle tiers
# .dd-header-headline
# tier_headlines = soup.select(".dd-header-headline")
# for tier in tier_headlines:
#     print(tier.text.strip())

# same as loop above just as a list this time in a loop - more readable
tier_headlines = [tier.text.strip() for tier in soup.select(".dd-header-headline")]

# product names
# dd-image-box-caption
product_names = soup.select(".dd-image-box-caption")
stripped_product_names = [prodname.text.strip() for prodname in product_names]

# product price
product_prices = [name.split()[1] for name in tier_headlines if name.startswith("Pay")]


# data structure thought process:
# - tier name and price
#     - product 1
#     - product 2
# - tier2 name and price
#     - product 1
#     - product 2
# 

# This is data structure use to store bundle info
# tiers = {
#     "tier1": {
#         "price": 500,
#         "products": [
#             "name1",
#             "name2"
#         ]
#     },
#     "tier2": {
#         "price": 500,
#         "products": [
#             "name1",
#             "name2"
#         ]
#     }
# }


# Common access pattern
# for tiername,tierinfo in tiers.items():
#      print(tiername)
#      print("priced at", tierinfo['price'])
#      print("products:")
#      print(", ".join(tierinfo['products']))
#      print("\n\n")