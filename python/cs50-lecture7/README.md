### the examples below demonstrate CRUD (Create Read Update Delete)


import a csv into sqlite:
`.import 'favorite_tv_shows.csv' shows`

check schema that was auto-created: `.schema`

SQL query to see what is in the db: `SELECT title FROM shows;`

see entire table in db: `SELECT * FROM shows;`

SQL query to find most popular shows in the database:
`SELECT UPPER(title), COUNT(title) FROM shows GROUP BY UPPER(title) ORDER BY COUNT(title) DESC LIMIT 10;`

same as above but trimming the string entered by the user: 
`sqlite> SELECT UPPER(TRIM(title)), COUNT(title) FROM shows GROUP BY UPPER(TRIM(title)) ORDER BY COUNT(title) DESC LIMIT 10;`


insert a new entry into the table: ` INSERT INTO shows (timestamp, title, genres) VALUES("now", "The Muppet Show", "Comedy, Musical");`
now you can see that by querying for the title of it: `SELECT * FROM shows WHERE title LIKE "%Muppet%";`  add a genre with update: `UPDATE shows SET genres = "Comedy, Drama, Musical" WHERE title = "The Muppet Show";`

delete a show: `DELETE FROM shows WHERE title LIKE "Friends";`


## Relational Databases Concepts
![](relational-db-example.png)

- each show has an ID
- two tables now
- `favorites.py` has been updated to know create the two tables for us

selects the title that has a genre of "comedy": `SELECT title FROM shows WHERE id IN(SELECT show_id FROM genres WHERE genre = "comedy");`





save the db with the table that was auto-created from before:
`.save shows.db`