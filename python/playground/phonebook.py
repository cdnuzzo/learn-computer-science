# to learn about dictionaries
import csv

from cs50 import get_string

# with keyword will automatically close file for me
with file = open("phonebook.csv" , "a")

    name = get_string("Name: ")
    number = get_string("Number: ")

    writer = csv.writer(file)

    writer.writerow([name, number])
