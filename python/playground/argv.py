# from sys import argv
import sys

# if len(argv) == 2:
#     print(f"what up, {argv[1]}")
# else:
#     print("what up, world")


if len(sys.argv) != 2:
    print("missing your command line argument")
    sys.exit(1)

print(f"hello, {sys.argv[1]}")
sys.exit(0)