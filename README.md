The goal of this is to track and provide a free and open curriculum to learning Computer Science in order to become a successful Computer Systems Engineer.

---


MIT 6.001 Intro to Computer Science in Python

MIT 6.002 Intro to Computational Thinking & Data Science

MIT 6.004 Computation Structures

MIT 6.003 Computer System Engineering

MIT 6.824 Distributed Systems

MIT 6.828 Operating System Engineering

Georgia Tech Intro to Operating Systems

Georgia Tech Advanced Operating Systems

U of Wash  Computer Networks [video lectures](https://media.pearsoncmg.com/ph/streaming/esm/tanenbaum5e_videonotes/tanenbaum_videoNotes.html)

MIT 6.851 Advanced Data Structures

MIT 6.858 Computer Systems Security


Linux Kernel internals

BSD

Go

[Python](/python/README.md)
- [Beginner Lecture from CS50](https://youtu.be/kM4oZTJaO8k)
- [Intermediate Python Course](https://youtu.be/HGOBQPFzWKo)

C

[CPython](https://pg.ucsd.edu/cpython-internals.htm)

eBPF

Databases
- [SQL](https://youtu.be/ZX2T7slE_9I)

### Frameworks
Flask (Udemy & CS50)



### Specialties

U of I - Cloud Computing

MIT 15.512 - Blockchain

Embedded Systems

Advanced Cryptography

Quantum Computing

Teaching Computer Science

Hacking 
- Kali Linux
