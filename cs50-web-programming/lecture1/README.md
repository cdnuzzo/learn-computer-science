I would suggest always using git when doing programming. Be both verbose and succinct in your git commit messages. Find a balance. You will thank yourself later.

## useful commands:
- `git clone`
- `git status`
- `git add`
- `git commit`
- `git push`
- `git log`


## branching commands:
- `git branch`
- `git checkout -b [branch-name]`  Creats a new branch with that branch name chose.
- `git checkout`
- `git merge [branch-name]` This will merge the currenct branch with branch name provided.
- 


I suggest you search in your web search of choice to find what those do. The official git docs are a great place to start.