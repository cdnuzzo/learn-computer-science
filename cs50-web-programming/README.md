The purpose of this directory is to reenforce basic to intermediate web programming concepts from CS50's Web Programming with Python and JavaScript 2020 course, found on [YouTube](https://www.youtube.com/playlist?list=PLhQjrBD2T380xvFSUmToMMzERZ3qB5Ueu).


I will provide my personal comments along the way in each directory's README.


Each lecture will be organized with `lecture#` starting with `0` because programming.


- `lecture0`: HTML, CSS, Sass, Bootstrap concepts
- `lecture1`: git
- `lecture2`: Python
- `lecture3`: Django


If you are brand new to programming and do not have a "code editor" of choice. I recommend either Vim or [VSCodium](https://vscodium.com/).


### Additioally, I recommend [CS50 course](https://youtu.be/Tpl7k8IOT6E) before this one for all the theory.