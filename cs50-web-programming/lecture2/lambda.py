people = [
    {"name": "Harry", "house": "Gryffindor"},
    {"name": "Cho", "house": "Ravenclaw"},
    {"name": "Draco", "house": "Slytherin"}
]

# how to do the sort without lambda
# def f(person):
#     return person["name"]

people.sort(key=lambda person: person["name"])

print(people)