# imports functions.py file with the square function
from functions import square

for i in range(10):
    print(f"The square of {i} is {square(i)}")